var express = require('express');
var router = express.Router();
const rps = require('../controllers/rpsController');
const roomValidation = require('../middlewares/roomValidation');

router.post('/fight/:roomId',roomValidation, rps.fight)

module.exports = router;
