var express = require('express');
var router = express.Router();
const users = require('./users');
const rps = require('./rps');

router.use(users);
router.use(rps);

module.exports = router;
