const { GameHistoryDetail } = require('../models')

function getRoundResult(gameDetail1, gameDetail2) {
    //logic here

    return 'User with username player1 Win';
}

function getOverallResult(gameDetailsPerRoom) {
    //logic here

    return 'User with username player1 Win';
}

function getRPSResult(gameDetailsPerRoom) {
    const dataRounds = [];
    let round = 1;
    for (let i = 0; i < gameDetailsPerRoom.length; i+=2) {
        dataRounds.push({
            round,
            result: getRoundResult(gameDetailsPerRoom[i], gameDetailsPerRoom[i + 1]),
            choices: [
                {
                    user_id: gameDetailsPerRoom[i].user_id,
                    choice: gameDetailsPerRoom[i].choice
                },
                {
                    user_id: gameDetailsPerRoom[i+1].user_id,
                    choice: gameDetailsPerRoom[i+1].choice
                }
            ]
        });
        round++;
    }

    return {
        overallResult: getOverallResult(gameDetailsPerRoom),
        rounds: dataRounds
    };
}

module.exports = {
    fight: async (req, res, next) => {
        const { roomId } = req.params;
        const { choice } = req.body;
        const { id } = req.user;

        await GameHistoryDetail.create({ room_id: roomId, user_id: id, choice });

        let gameDetailsPerRoom = await GameHistoryDetail.findAll({ room_id: roomId });
        if (gameDetailsPerRoom.length % 2 === 1) {
            const intervalId = setInterval(async () => {
                gameDetailsPerRoom = await GameHistoryDetail.findAll({ room_id: roomId });

                if (gameDetailsPerRoom.length % 2 === 0) {
                    clearInterval(intervalId);

                    res.json(getRPSResult(gameDetailsPerRoom));
                }
            }, 5000);
        } else {
            res.json(getRPSResult(gameDetailsPerRoom));
        }
    },
}