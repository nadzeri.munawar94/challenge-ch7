const { GameHistoryDetail } = require('../models')

module.exports = async (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.json({ message: 'Silakah login terlebih dahulu' });
    }
    
    const { roomId } = req.params;
    const { id } = req.user;
    const gameDetailsPerRoom = await GameHistoryDetail.findAll({ room_id: roomId });
    if (gameDetailsPerRoom.length === 6) { // kalau data sudah 6
        return res.json({ message: 'Room sudah tidak bisa dipakai' });
    }

    if (gameDetailsPerRoom.length < 2) { // kalau sudah 2 player
        return next();
    }

    if (!(gameDetailsPerRoom[0].user_id === id || gameDetailsPerRoom[1].user_id === id)) { // kalau sudah 2 player
        return res.json({ message: 'Gunakan room lain' });
    }

    next();
}